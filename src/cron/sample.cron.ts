/**
 *
 **/

import logger from './../utils/logger';
import schedule from 'node-schedule';

export default class SampleCron {

  // https://crontab.guru
  interval = '*/1 * * * *'; //'@hourly';
  name = 'sample_cron';

  constructor() {
    schedule.scheduleJob(this.name, this.interval, this.job);
  }

  // Initiate the cron
  public job() {
    logger.info('Sample cron called');
  }
}
