import * as express from 'express';
import Controller from '../interfaces/controller.interface';

class HomeController implements Controller {

  public path = '/';
  public router = express.Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(this.path, this.home);
  }

  /**
   *
   * Home route
   * 
   **/
  private home = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.json({
      title: 'MAKE101 API', 
      version: require('../../package.json').version
    });
  }

}

export default HomeController;
