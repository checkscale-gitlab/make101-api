var ManagementClient = require('auth0').ManagementClient;
import { config } from '../config';
import logger from '../utils/logger';

let auth0 = new ManagementClient({
  domain: config.auth0.domain,
  clientId: config.auth0.clientId,
  clientSecret: config.auth0.clientSecret
});

// User interface
interface IAuth0User {
  user_id?: string
  email?: string
  nickname?: string
  picture?: string
  created_at?: Date
  email_verified?: boolean
  app_metadata?: {
    slug?: string
    account_plan?: string
    account_plan_end?: Date
    admin?: boolean
  }
  user_metadata?: {
    firstname?: string
    lastname?: string
    bio?: string
  }
}

//
// interface IGetUsersParams {
//   per_page?: number
//   page?: number
//   include_totals?: boolean
//   q?: object
// }

/**
 *
 * Fetch a user from auth0
 *
 **/
const GetAuth0User = async (sub: string): Promise<any> => {
  logger.debug(`[ auth0.service ] GetAuthOUser - Fetching user ${sub}`)
  return await auth0.getUser({ id: `${sub}` });
}

/**
 *
 * Update user profile at auth0;
 * @param sub
 * @param data
 *
 **/
const UpdateAuth0User = async (sub: string, data: IAuth0User): Promise<any> => {
  console.log('updateAuthUser', data);
  const params = { id: sub }
  let result: any;

  try {
    await auth0.updateUser(params, data);
    logger.info(`[ auth0.service ] UpdateAuth0User - user core data updated`); 

    if (Object.keys(data.user_metadata).length>0) {
      await auth0.updateAppMetadata(params, data.user_metadata);
      logger.info(`[ auth0.service ] UpdateAuth0User - user app_metadata updated`); 
    }
    
    if (Object.keys(data.app_metadata).length>0) {
      await auth0.updateUserMetadata(params, data.app_metadata);
      logger.info(`[ auth0.service ] UpdateAuth0User - user user_metadata updated`); 
    }
    result=true;
  } catch (err) {
    logger.warn(`[ auth0.service ] UpdateAuth0User - Error Updating user. `); 
    logger.warn(err); 
    result = false; 
  }

  return result;
}

/**
 *
 * Helper function to generate a unique nickname.
 * @param nickname
 *
 **/
 const GenerateUniqueNickname = async (nickname: string): Promise<string> => {

  logger.debug(`[ user.service - generateUniqueNickname ] Init ${nickname}`);

  let i = 0;
  let valid = false;
  let newNickname = nickname;
  let error = false;

  while (!valid && !error) {
    logger.debug(`[ user.service - generateUniqueNickname ] Check if nickname is already taken: ${newNickname}. `);
    try {
      const userCount = await auth0.getUsers({ nickname: nickname });
      if ((userCount === 0) || !userCount ) {
        logger.debug(`[ user.service - generateUniqueNickname ] Nickname is not yet taken. `);
        valid = true;
      } else {
        logger.debug(`[ user.service - generateUniqueNickname ] Nickname is already taken. `);
        i++;
        newNickname = nickname + i;
        logger.debug(`[ user.service - generateUniqueNickname ] New nickname is ${newNickname}. `);
      }
    } catch (err) {
      logger.warn(`[ user.service - generateUniqueNickname ] error: `, err);
      error = true;
    }

    if (i > 50) {
      error = true;
      logger.warn(`[ user.service - generateUniqueNickname ] Tried 50 names,aborting.`);
    }
  }

  if (error) { newNickname = null; }

  logger.debug(`[ user.service - generateUniqueNickname ] Result: ${newNickname}. `);
  return newNickname;
}

/**
 * 
 * 
 * @param userId 
 */
const DeleteAuth0User = async (sub: string): Promise<any> => {
  logger.info(`Deleting user with id: ${sub}`);
  return await auth0.deleteUser({ id: sub });
}

export {
  IAuth0User,
  DeleteAuth0User, 
  GenerateUniqueNickname,
  GetAuth0User,
  UpdateAuth0User
}
